### TAS_AICT18

This repository contains the code which has been used in order to generate the simulation results done for our article: 

Bonnefoi, R.; Moy, C.; Palicot, J.; Nafkha A. �Low-Complexity Antenna  Selection for Minimizing the Power Consumption of a MIMO Base Station�,  AICT 2018, July 2018.

A version is available at: https://hal.archives-ouvertes.fr/hal-01826578/document

### Short description 

This repository contains two main files:

* The first one : main\_NBS.m  compares the performance of the NBS strategy with an exhaustive search (Fig. 2)
* The second : main\_SC.m Evaluates the performance of the proposed stopping criteria. (Figure 4,5 and 6 in our paper)

### License

[MIT Licensed](https://mit-license.org/) (file [LICENSE.txt](LICENSE.txt)).